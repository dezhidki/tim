timApp.document.translation package
===================================

Submodules
----------

timApp.document.translation.routes module
-----------------------------------------

.. automodule:: timApp.document.translation.routes
    :members:
    :undoc-members:
    :show-inheritance:

timApp.document.translation.synchronize\_translations module
------------------------------------------------------------

.. automodule:: timApp.document.translation.synchronize_translations
    :members:
    :undoc-members:
    :show-inheritance:

timApp.document.translation.translation module
----------------------------------------------

.. automodule:: timApp.document.translation.translation
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.document.translation
    :members:
    :undoc-members:
    :show-inheritance:
